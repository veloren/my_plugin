use veloren_plugin_rt::{*, api::{*, event::*}};

/// Handle the plugin load event
#[event_handler]
pub fn on_load(load: PluginLoadEvent) {
    emit_action(Action::Print(String::from("Hello, Veloren!")));
}

/// The global state of the plugin
#[global_state]
#[derive(Default)]
struct State {
    // The number of pings that have occurred so far
    ping_count: u64,
}

/// Handle `/ping` commands
#[event_handler]
pub fn on_command_ping(chat_cmd: ChatCommandEvent, state: &mut State) -> Result<Vec<String>, String> {
    // Keep track of the number of pings that have occurred so far
    state.ping_count += 1;
    // Display the ping count to the user
    Ok(vec![format!("Pong! The total number of pings so far is {}", state.ping_count)])
}
